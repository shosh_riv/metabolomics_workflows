# Metabolomics workflows

Ready-to-use workflows for processing of metabolomic data from different types of measurements. Workflows are developed for a specific project, and then generalized and moved to this repo for broader use. Will be added to irregularly when I perform or are given a new measurement requiring me to develop another workflow.